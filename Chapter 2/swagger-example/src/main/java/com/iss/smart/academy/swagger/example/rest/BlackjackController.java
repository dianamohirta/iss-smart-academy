package com.iss.smart.academy.swagger.example.rest;

import com.iss.smart.academy.swagger.example.service.BlackjackService;
import com.iss.smart.academy.swagger.example.singleton.Status;
import io.swagger.api.BlackjackApi;
import io.swagger.model.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BlackjackController implements BlackjackApi {

    Status status = Status.getInstance();

    @Autowired
    private BlackjackService blackjackService;

    @Override
    public ResponseEntity<byte[]> blackjackPlayPost() {

        byte[] image = blackjackService.play();

        MultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.add("Content-Disposition", "inline");
        headers.add("Winner", String.valueOf(status.isWinner()));
        headers.add("Count", String.valueOf(status.getCount()));

        return new ResponseEntity<>(image, headers, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<io.swagger.model.Status> blackjackStatusGet() {

        io.swagger.model.Status status = blackjackService.getStatus();
        return new ResponseEntity<>(status, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<Result> blackjackStopPost() {

        Result result = blackjackService.stop();
        return new ResponseEntity<>(result, HttpStatus.OK);

    }
}
