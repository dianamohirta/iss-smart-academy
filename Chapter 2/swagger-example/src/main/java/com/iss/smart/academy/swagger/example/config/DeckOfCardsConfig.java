package com.iss.smart.academy.swagger.example.config;

import com.deck.of.cards.ApiClient;
import com.deck.of.cards.api.DefaultApi;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DeckOfCardsConfig {

    private String basePath;

    public DeckOfCardsConfig(@Value("${deck.of.cards.basePath}") String basePath) {
        this.basePath = basePath;
    }

    private ApiClient getApiClient() {

        ApiClient apiClient = new ApiClient();
        apiClient.setBasePath(basePath);

        return apiClient;
    }

    @Bean
    public DefaultApi getDefaultApi() {
        return new DefaultApi(getApiClient());
    }

    public String getBasePath() {
        return basePath;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }
}
