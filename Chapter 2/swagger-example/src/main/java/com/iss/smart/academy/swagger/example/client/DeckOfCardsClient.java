package com.iss.smart.academy.swagger.example.client;

import com.deck.of.cards.api.DefaultApi;
import com.deck.of.cards.model.CreateDeckResponse;
import com.deck.of.cards.model.DrawCardsResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;

@Component
public class DeckOfCardsClient {

    private static final Logger LOG = LoggerFactory.getLogger(DeckOfCardsClient.class);
    @Autowired
    DefaultApi defaultApi;

    public String createDeck() {
        CreateDeckResponse createDeckResponse = defaultApi.apiDeckNewShuffleGet("6", "Spring-Service");
        LOG.info("Draw Cards response: {}", createDeckResponse);

        return createDeckResponse.getDeckId();
    }

    public byte[] getImage(String cardName) {
        return defaultApi.staticImgCardNameGet(cardName).getBytes(StandardCharsets.ISO_8859_1);
    }

    public DrawCardsResponse drawCards(String deckId) {

        DrawCardsResponse drawCardsResponse = defaultApi.apiDeckDeckIdDrawGet(deckId, 1, "Spring-Service");
        LOG.info("Draw Cards response: {}", drawCardsResponse);

        return drawCardsResponse;
    }
}
