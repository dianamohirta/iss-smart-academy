package com.iss.smart.academy.swagger.example.service;

import io.swagger.model.Result;
import io.swagger.model.Status;

public interface BlackjackService {

    byte[] play();

    Result stop();

    Status getStatus();
}
