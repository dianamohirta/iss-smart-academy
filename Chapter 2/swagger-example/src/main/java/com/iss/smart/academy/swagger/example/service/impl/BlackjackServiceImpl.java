package com.iss.smart.academy.swagger.example.service.impl;

import com.deck.of.cards.model.Card;
import com.deck.of.cards.model.DrawCardsResponse;
import com.iss.smart.academy.swagger.example.client.DeckOfCardsClient;
import com.iss.smart.academy.swagger.example.service.BlackjackService;
import com.iss.smart.academy.swagger.example.singleton.Status;
import io.swagger.model.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class BlackjackServiceImpl implements BlackjackService {

    private static final Logger LOG = LoggerFactory.getLogger(BlackjackServiceImpl.class);
    private static final Map<String, Integer> specialCards = new HashMap<>();

    static {
        specialCards.put("KING", 14);
        specialCards.put("QUEEN", 13);
        specialCards.put("JACK", 12);
        specialCards.put("ACE", 11);
    }

    private Status status = Status.getInstance();
    @Autowired
    private DeckOfCardsClient deckOfCardsClient;

    public byte[] play() {

        if (status.getDeckId() == null || status.isWinner()) {

            status.setDeckId(deckOfCardsClient.createDeck());
            status.setWinner(false);
            status.setCount(0);

        }

        DrawCardsResponse drawCardsResponse = deckOfCardsClient.drawCards(status.getDeckId());
        status.setRemaining(
                drawCardsResponse.getRemaining()
        );

        String value = drawCardsResponse.getCards().get(0).getValue();
        int count = status.getCount() + extractCardValue(value);
        status.setCount(count);

        if (count > 21) {
            status.setWinner(false);
            status.setCount(0);
        }
        if (count == 21)
            status.setWinner(true);

        return deckOfCardsClient.getImage(
                drawCardsResponse.getCards().get(0).getCode() + ".png"
        );
    }

    public Result stop() {

        if (status.getDeckId() == null)
            return null;

        DrawCardsResponse drawCardsResponse = deckOfCardsClient.drawCards(status.getDeckId());
        Card card = drawCardsResponse.getCards().get(0);
        String value = card.getValue();

        int newCount = status.getCount() + extractCardValue(value);

        if (status.getCount() > 21 || Math.abs(status.getCount() - 21) > Math.abs(newCount - 21))
            status.setWinner(false);
        else
            status.setWinner(true);

        status.setDeckId(null);

        return new Result()
                .winner(status.isWinner())
                .lastCard(value);
    }

    private int extractCardValue(String value) {
        if (specialCards.containsKey(value))
            return specialCards.get(value);
        return Integer.parseInt(value);
    }

    public io.swagger.model.Status getStatus() {

        return new io.swagger.model.Status()
                .count(status.getCount())
                .remaining(status.getRemaining());
    }

}
