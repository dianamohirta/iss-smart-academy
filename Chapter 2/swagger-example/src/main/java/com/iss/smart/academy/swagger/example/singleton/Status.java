package com.iss.smart.academy.swagger.example.singleton;

public class Status {

    private static Status instance;

    private boolean winner;
    private int count;
    private int remaining;
    private String deckId;

    public static synchronized Status getInstance() {
        if (instance == null) {
            instance = new Status();
        }
        return instance;
    }

    public boolean isWinner() {
        return winner;
    }

    public void setWinner(boolean winner) {
        this.winner = winner;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getDeckId() {
        return deckId;
    }

    public void setDeckId(String deckId) {
        this.deckId = deckId;
    }

    public int getRemaining() {
        return remaining;
    }

    public void setRemaining(int remaining) {
        this.remaining = remaining;
    }
}
